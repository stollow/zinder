import { MatchedProfil } from './../../models/matchedProfil.model';
import { Match } from './../../models/match.model';
import { Component, OnInit, Input } from '@angular/core';
import { Profil } from 'src/app/models/profil.model';

@Component({
  selector: 'matchStats',
  templateUrl: './matchStats.component.html',
  styleUrls: ['./matchStats.component.css']
})
export class MatchStatsComponent implements OnInit {

  @Input() matchedProfils: MatchedProfil[];

  constructor() { }

  ngOnInit() {
  }

}
