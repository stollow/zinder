import { Match } from './../../../models/match.model';
import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';

@Component({
  selector: 'matchButtons',
  templateUrl: './matchButtons.component.html',
  styleUrls: ['./matchButtons.component.css']
})
export class MatchButtonsComponent implements OnInit {

  @Output() isMatch = new EventEmitter<Match>();
  @Input() profilId: string;
  isChoiced = false;
  constructor() { }

  ngOnInit() {
  }

  match() {
    const match = new Match(this.profilId, true);
    this.isMatch.emit(match);
    this.isChoiced = true;
  }

  nope() {
    const match = new Match(this.profilId, false);
    this.isMatch.emit(match);
    this.isChoiced = true;
  }
}
