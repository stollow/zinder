import { Interet } from './../../../models/interet.model';
import { ProfilsService } from 'src/app/services/profils.service';
import { Component, OnInit, Input } from '@angular/core';
import { Profil } from 'src/app/models/profil.model';

@Component({
  selector: 'profilPresentation',
  templateUrl: './profilPresentation.component.html',
  styleUrls: ['./profilPresentation.component.css']
})
export class ProfilPresentationComponent implements OnInit {

  @Input() profil: Profil;
  interet: Interet[] = [];
  @Input() filteredInteret: string;

  constructor(private profilsService: ProfilsService) { }

  ngOnInit() {
    this.profilsService.getInteret().subscribe(response => {
      this.interet = response;
    });
  }

  getInteret(): Interet[] {
      const printInterets = this.interet.filter(filteredResponse => {
        if (this.filteredInteret) {
          return this.profil.interets.includes(filteredResponse.id) && filteredResponse.nom === this.filteredInteret;
        } else {
          return this.profil.interets.includes(filteredResponse.id);
        }
      });
      return printInterets;
  }
}
