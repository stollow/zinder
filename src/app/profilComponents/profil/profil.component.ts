import { MatchedProfil } from './../../models/matchedProfil.model';
import { Match } from './../../models/match.model';
import { Component, OnInit, Input, Output , EventEmitter } from '@angular/core';
import { Profil } from 'src/app/models/profil.model';
import { ProfilsService } from 'src/app/services/profils.service';

@Component({
  selector: 'profil',
  templateUrl: './profil.component.html',
  styleUrls: ['./profil.component.css']
})
export class ProfilComponent implements OnInit {

  @Input() profil: Profil;
  @Output() matchsEmitter = new EventEmitter<Match>();
  @Input() filteredInteret: string;

  constructor(private profilsService: ProfilsService) { }

  ngOnInit() {
  }


  setMatch(match: Match): void {
    this.matchsEmitter.emit(match);
  }

}

