import { Interet } from './../../models/interet.model';
import { MatchedProfil } from './../../models/matchedProfil.model';
import { Match } from './../../models/match.model';
import { Component, OnInit } from '@angular/core';
import { Profil } from 'src/app/models/profil.model';
import { ProfilsService } from 'src/app/services/profils.service';

@Component({
  selector: 'profilsLayout',
  templateUrl: './profilsLayout.component.html',
  styleUrls: ['./profilsLayout.component.css']
})
export class ProfilsLayoutComponent implements OnInit {

  listProfils: Profil[] = [];
  matchedProfils: MatchedProfil[] = [];
  matchs: Match[];
  isLoading = true;
  interets: Interet[];
  filteredInteret: string;

  constructor(private profilsService: ProfilsService) { }

  ngOnInit() {
    this.profilsService.getProfils().subscribe(response => {
      this.listProfils = response.profils;
    });
    this.profilsService.getMatchs().subscribe(response => {
      this.matchs = response;
      const that = this;
      this.matchs.forEach(match => {
        that.setMatchs(match, true);
      });
      this.isLoading = false;
    });
    this.profilsService.getInteret().subscribe(response => {
      this.interets = response;
    });
  }

  setMatchs(match: Match, isInit = false): void {
      const matchedProfil = new MatchedProfil(match, this.listProfils.find(profil => profil.id === match.profil));
      this.matchedProfils.push(matchedProfil);
      if (!isInit) {
      this.profilsService.addMatch(match).subscribe();
      }
      this.filteredMatchedProfil();
  }

  filteredMatchedProfil() {
    this.listProfils = this.listProfils
    .filter(filteredProfil => !this.matchedProfils
    .find(filteredMatchedProfil => filteredMatchedProfil.profil.id === filteredProfil.id));
  }

}
