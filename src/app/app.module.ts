import { MatchStatsComponent } from './profilComponents/matchStats/matchStats.component';
import { HttpClientModule } from '@angular/common/http';
import { ProfilsService } from './services/profils.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';
import { ProfilPresentationComponent } from './profilComponents/profil/profilPresentation/profilPresentation.component';
import { MatchButtonsComponent } from './profilComponents/profil/matchButtons/matchButtons.component';
import { ProfilComponent } from './profilComponents/profil/profil.component';
import { ProfilsLayoutComponent } from './profilComponents/profilsLayout/profilsLayout.component';

@NgModule({
  declarations: [
    AppComponent,
    ProfilComponent,
    ProfilsLayoutComponent,
    ProfilPresentationComponent,
    MatchButtonsComponent,
    MatchStatsComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
  ],
  providers: [
    ProfilsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
