import { Match } from './match.model';
import { Profil } from './profil.model';

export class MatchedProfil {
    profil: Profil;
    match: Match;


    constructor(match: Match, profil: Profil) {
        this.match = match;
        this.profil = profil;
    }
}