export class Match {
    profil: string;
    match: boolean;


    constructor(profilId: string, isMatch: boolean) {
        this.profil = profilId;
        this.match = isMatch;
    }
}
