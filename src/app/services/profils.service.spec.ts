/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { ProfilsService } from './profils.service';

describe('Service: Profils', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfilsService]
    });
  });

  it('should ...', inject([ProfilsService], (service: ProfilsService) => {
    expect(service).toBeTruthy();
  }));
});
