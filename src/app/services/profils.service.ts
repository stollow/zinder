import { Match } from './../models/match.model';
import { ListProfil } from './../models/listProfil.model';
import { Profil } from './../models/profil.model';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Interet } from '../models/interet.model';

@Injectable({
  providedIn: 'root'
})
export class ProfilsService {

constructor(private http: HttpClient) { }


public getProfils(): Observable<ListProfil> {
  return this.http.get<ListProfil>('http://localhost:8088/zinder/profils',
  { observe: 'response' })
  .pipe(
    map(response => {
      const data: ListProfil = response.body;
      return data;
    }));
  }

  public addMatch(match: Match): Observable<Match> {
    return this.http.post<Match>(`http://localhost:8088/zinder/profils/${match.profil}/match`, match);
  }

  public getMatchs(): Observable<Match[]> {
    return this.http.get<Match[]>('http://localhost:8088/zinder/matchs',
    { observe: 'response' })
    .pipe(
      map(response => {
        const data: Match[] = response.body;
        return data;
      }));
    }

  public getInteret(): Observable<Interet[]> {
    return this.http.get<Interet[]>('http://localhost:8088/zinder/interets',
    { observe: 'response' })
    .pipe(
      map(response => {
        const data: Interet[] = response.body;
        return data;
      }));
  }
}
